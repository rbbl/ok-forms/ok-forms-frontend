terraform {
  required_providers {
    auth0 = {
      source  = "auth0/auth0"
      version = "~> 0.34.0"
    }
  }
  backend "kubernetes" {
    secret_suffix  = "state"
    config_path    = "~/.kube/config"
    config_context = "hetzner-default"
    namespace      = "tf-state"
  }
}

provider "auth0" {}

resource "auth0_client" "frontend" {
  name                       = "Frontend Application"
  app_type                   = "spa"
  allowed_logout_urls        = ["http://localhost:4200"]
  callbacks                  = ["http://localhost:4200"]
  web_origins                = ["http://localhost:4200"]
  oidc_conformant            = true
  token_endpoint_auth_method = "none"
  grant_types                = [
    "authorization_code",
    "implicit",
    "refresh_token"
  ]
}
